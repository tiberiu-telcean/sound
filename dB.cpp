#include <cmath>
#include "dB.h"

double dB_to_amplitude(double l)
{
  return pow(10, l/20);
}
