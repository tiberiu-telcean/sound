#pragma once
#include "buffer.h"
#include "client.h"
#include "dB.h"
#include "defs.h"
#include "instrument.h"
#include "mutex.h"

class SynthController;

class Synth : public Source {
  bool m_try_closing = false;

  int n_frames;
  int at_frame;
  Buffer<float> *play_buffer;
  Instrument *m_instrument;
  std::vector<note> m_notes;
  volume m_volume;
  SynthController *m_controller;
  struct port * m_midi_in_port;

  void read_midi();

  void on_process(struct spa_io_position *position);
  void do_quit(int);

  int render_one_frame(int i, Buffer<float> &buffer);
public:
  Synth(SynthController *);
  void set_volume(double x) { m_volume = dB_to_amplitude(x); }
  void set_volume(int x) { m_volume = dB_to_amplitude(x); }
  void window_closed() { m_try_closing = true; }
  void key_pressed(int);
  void key_released(int);
};
