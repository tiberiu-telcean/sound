#include <cstdint>
#include <cstring>

#include "dB.h"
#include "saturator.h"

void Saturator::on_process(struct spa_io_position *position)
{
  async_update();

  uint32_t n_samples = position->clock.duration;
  float *input = (float *)pw_filter_get_dsp_buffer(m_input_port, n_samples);
  Wave out(0);
  float *output = (float *)pw_filter_get_dsp_buffer(m_output_port, n_samples);

  if(!input || !output)
    return;

  QMutexLocker l(&m_parameter_lock);
  for(unsigned i = 0; i < n_samples; i++)
  {
    float original = input[i];
    input[i] *= dB_to_amplitude(m_preamp);
    if(input[i] > m_threshold)
      input[i] = (input[i] - m_threshold) / m_ratio + m_threshold;
    if(input[i] < -m_threshold)
      input[i] = -m_threshold - (-input[i] - m_threshold) / m_ratio;
    input[i] *= dB_to_amplitude(m_gain) * m_mix;
    input[i] += original * (1 - m_mix);
  }

  memcpy(output, input, n_samples * sizeof(float));
}

void Saturator::set_ratio(float ratio)
{
  QMutexLocker l(&m_parameter_lock);
  m_ratio = ratio;
}

void Saturator::set_threshold(float threshold)
{
  QMutexLocker l(&m_parameter_lock);
  m_threshold = threshold;
}
