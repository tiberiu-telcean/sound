#pragma once
#include <memory>

#include "defs.h"
#include "effect.h"
#include "mutex.h"

class CompressorController;

class Compressor : public Effect
{
  public:
  Compressor(CompressorController *controller);
  void on_process(struct spa_io_position *) override;

  void set_attack(float);
  void set_ratio(float);
  void set_threshold(float);
  private:
  factor m_ratio = 5;
  std::unique_ptr<float[]> env(const float *a, uint32_t size);
  pulse r(pulse x);
  pulse n(float x, amplitude e);
  seconds m_attack = .003;
  amplitude m_threshold = .4;

  struct port *m_sidechain_port;
  CompressorController *m_controller;
};
