#include <cstdio>
#include <cassert>

#include <pipewire/pipewire.h>
#include <qapplication.h>

#include "main.h"
#include "synth.h"
#include "qt.h"

FILE *output;
QApplication *app;

int main(int argc, char **argv, char**)
{
  if (argc > 1) {
    assert(output = fopen(argv[1], "ab"));
  } else {
    output = fopen("/dev/null", "wb");
  }

  app = new QApplication(argc, argv);
  pw_init(&argc, &argv);

  fprintf(stdout, "Compiled with libpipewire %s\n"
              "Linked with libpipewire %s\n",
                      pw_get_headers_version(),
                      pw_get_library_version());

  Win main_window;

  int result = app->exec();
  pw_deinit();

  return result;
}
