#include <cstring>
#include <QCoreApplication>
#include <vector>

#include "compressor.h"
#include "dB.h"
#include "main.h"
#include "qcoreapplication.h"
#include "qt.h"

Compressor::Compressor(CompressorController *controller) :Effect("compressor"), m_controller(controller)
{
  m_sidechain_port = static_cast<struct port *>(pw_filter_add_port(m_filter,
      PW_DIRECTION_INPUT,
      PW_FILTER_PORT_FLAG_MAP_BUFFERS,
      sizeof(struct port),
      pw_properties_new(
        PW_KEY_FORMAT_DSP, "32 bit float mono audio",
        PW_KEY_PORT_NAME, "sidechain",
        NULL),
      NULL, 0));
}

pulse Compressor::r(pulse a)
{
  QMutexLocker l(&m_parameter_lock);
  if(a>m_threshold)
      return m_threshold+(a-m_threshold)/m_ratio;
  else
      return a;
}

pulse Compressor::n(float x, amplitude e)
{
  return x*r(e);
}

std::vector<const float *> blockhop(const float *a, uint32_t size)
{
  std::vector<const float *> result;
  for(unsigned i = 0; i <= size-frame_size; i += frame_size)
  {
    result.push_back(&a[i]);
  }
  return result;
}

void Compressor::on_process(struct spa_io_position *position)
{
  async_update();

  uint32_t n_samples = position->clock.duration;
  float *wave = (float *)pw_filter_get_dsp_buffer(m_input_port, n_samples);
  float *out = (float *)pw_filter_get_dsp_buffer(m_output_port, n_samples);
  if(!wave || !out)
    return;

  float *sidechain = (float *)pw_filter_get_dsp_buffer(m_sidechain_port, n_samples);
  if(!sidechain)
    sidechain = wave;

  std::unique_ptr<float[]> e = env(sidechain, n_samples);
  for(unsigned i = 0; i < n_samples; i++)
  {
    out[i] = n(wave[i] * dB_to_amplitude(m_preamp), e[i]) * dB_to_amplitude(m_gain) * m_mix;
    out[i] += wave[i] * (1 - m_mix);
  }
}

void Compressor::set_attack(float attack)
{
  QMutexLocker l(&m_parameter_lock);
  m_attack = attack;
}

void Compressor::set_threshold(float threshold)
{
  QMutexLocker l(&m_parameter_lock);
  m_threshold = threshold;
}

void Compressor::set_ratio(float ratio)
{
  QMutexLocker l(&m_parameter_lock);
  m_ratio = ratio;
}

std::unique_ptr<float[]> Compressor::env(const float *wave, uint32_t size)
{
  int n_frames = size/frame_size;
  if(size % frame_size)
    n_frames++;
  std::unique_ptr<float[]> e = std::make_unique<float[]>(n_frames * frame_size);
  memset(e.get(), 0, n_frames * frame_size * sizeof(float));
  for(unsigned i = 0; i < size; i++)
  {
    for(unsigned j = (i/frame_size)*frame_size; j < (i/frame_size + 1)*frame_size; j++)
    {
      if(j <= size && abs(wave[j]) > e[j])
        e[j] = abs(wave[j]);
    }
  }
  return e;
}
