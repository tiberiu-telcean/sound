#pragma once

#include <cmath>
#include <cstdint>
#include <iterator>

#define MATHF(exp) [](double x)->double{return exp;}

typedef float amplitude;
typedef amplitude* envelope_t;
typedef amplitude pulse; /**< one sample */
typedef pulse* wave_t;
typedef char* path;
typedef float hz;
typedef float wavelenght;
typedef float seconds;
typedef hz frequency;
typedef int semitones;
typedef int lenght; // lengHT
typedef float volume; /**< Volume in the final wave */
typedef double factor; /**< Generic number */
typedef double ang_vel; /**< Angular Velocity */
typedef float mod_const; /**< FM Modulation constant @see math.pdf */
typedef double (*osc_t)(double);

const hz sample_rate = 48000.0;
const lenght frame_size = 512;
const lenght hop_size = 512;
const hz A4 = 440.0; /**< Middle A */
// const semitones pause = 500; /**< If a note in a file is 500 semitones high, it will count as being a pause. */
const uint32_t stride = sizeof(float);

struct note {
  semitones pitch;
  seconds start;
  seconds length;
  bool should_be_removed = false;
  seconds get_end(void) const { return start+length; };
  bool operator<(note other) const {return get_end()<other.get_end();};
  bool operator>(note other) const {return get_end()>other.get_end();};
};
