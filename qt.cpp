#include <algorithm>
#include <cstdint>
#include <cstring>
#include <pipewire/core.h>
#include <QDial>
#include <QScrollArea>
#include <QSizePolicy>
#include <QStringBuilder>
#include <QThread>
#include <QTimer>
#include <QtWidgets/QLabel>
#include <QVBoxLayout>

#include "controllers.h"
#include "effect_controllers.h"
#include "instrument.h"
#include "main.h"
#include "oscillators.h"
#include "slider.h"
#include "synth.h"
#include "qt.h"

QWidget *Loader::createWidget(const QString &className, QWidget *parent, const QString &name)
{
  if(className == "Slider")
    return new Slider(parent);
  return QUiLoader::createWidget(className, parent, name);
}

template<typename T>
T Win::load_ui_file(const char *file, QWidget* parent)
{
  QFile ui_file(file);
  ui_file.open(QFile::ReadOnly);
  T result = m_loader.load(&ui_file, parent);
  ui_file.close();
  return result;
}

Track::Track(Controller *controller, const char *name, QVBoxLayout *effects_layout)
{
  source = controller;
  track_id = name;

  ui_group = new QGroupBox(QGroupBox::tr(name));
  layout = new QVBoxLayout();
  effects_layout->insertWidget(effects_layout->count() - 1, ui_group);
  ui_group->setFixedHeight(40); // TODO
  ui_group->setFixedHeight(40); // TODO
  ui_group->setLayout(layout);
  layout->setSpacing(5);
}

void Track::add_effect(std::pair<EffectController *, QWidget *> effect)
{
  effects.push_back(effect.first);
  ui_group->setFixedHeight(layout->spacing() + ui_group->height() + effect.second->height());
  layout->addWidget(effect.second);
  layout->addStretch(1);
}

Win::Win(QWidget *parent): QMainWindow(parent)
{
  m_loader.createWidget("Slider");
  myWidget = load_ui_file<QWidget *>("main.ui", this);
  myWidget->setVisible(true);

  m_play_button = myWidget->findChild<QPushButton *>("play");
  m_mixer = myWidget->findChild<QWidget *>("mixer");
  m_audio_devices = findChild<QComboBox *>("audio_outputs");
  m_effects_layout = findChild<QVBoxLayout *>("effects_layout");
  m_effects_layout->addStretch(1);
  m_tracks_layout = findChild<QHBoxLayout *>("tracks_layout");
  m_tracks_layout->addStretch(1);
  instrument = findChild<QComboBox *>("instrument");

  // FIXME : Why is this needed?
  myWidget->findChild<QWidget *>("effects_scroll_area")->setLayout(m_effects_layout);
  findChild<QWidget *>("tracks_scroll_area")->setLayout(m_tracks_layout);

  connect(m_play_button, &QPushButton::clicked, this, &Win::setup_connections);

  m_main_client_controller = new_main_client();

  m_tracks.push_back(Track(new_synth(), new_track(), m_effects_layout));
  m_tracks[0].add_effect(new_saturator());
  m_tracks[0].add_effect(new_compressor());

  oscilloscope = myWidget->findChild<QwtPlot *>("plot");
  oscilloscope->enableAxis(QwtPlot::xBottom, false);
  oscilloscope->enableAxis(QwtPlot::yLeft, false);
  oscilloscope->setAxisScale(QwtPlot::xBottom, 0, 1);
  oscilloscope->setAxisScale(QwtPlot::yLeft, -1, 1);
  curve = new QwtPlotCurve("Audio");
  curve->attach(oscilloscope);
  curve->setPen(QColorConstants::White, 2, Qt::SolidLine);

  QByteArray geometry = myWidget->saveGeometry();
  setCentralWidget(myWidget);
  restoreGeometry(geometry);
  show();
}

template<typename T>
T *Win::new_controller()
{
  T *controller = new T(this);
  QThread *thread = new QThread();

  controller->moveToThread(thread);
  thread->start();
  connect(thread, &QThread::finished, controller, &QObject::deleteLater);
  connect(m_play_button, &QPushButton::clicked, controller, &Controller::run);

  return controller;
}

template<typename T>
std::pair<T *, QWidget *> Win::new_effect(const char *ui_file)
{
  T *effect = new_controller<T>();
  QGroupBox *group = new QGroupBox(tr(effect->get_name()));
  QWidget *widget = load_ui_file<QWidget *>(ui_file, group);
  QVBoxLayout *layout = new QVBoxLayout();

  layout->addWidget(widget);
  group->setLayout(layout);
  // TODO : Figure out the size of the group widget
  group->setFixedHeight(widget->height() + 40);

  QDial *mix_dial = widget->findChild<QDial *>("mix_dial");
  QDial *gain_dial = widget->findChild<QDial *>("gain_dial");
  connect(mix_dial, &QDial::valueChanged, effect, &EffectController::set_mix);
  connect(gain_dial, &QDial::valueChanged, effect, &EffectController::set_gain);

  return std::pair<T *, QWidget *>(effect, group);
}

std::pair<CompressorController *, QWidget *> Win::new_compressor()
{
  std::pair<CompressorController *, QWidget *> effect = new_effect<CompressorController>("compressor.ui");
  CompressorController *compressor = effect.first;
  QWidget *widget = effect.second;

  QDial *threshold_dial = widget->findChild<QDial *>("threshold_dial");
  QDial *ratio_dial = widget->findChild<QDial *>("ratio_dial");
  connect(threshold_dial, &QDial::valueChanged, compressor, &CompressorController::set_threshold);
  connect(ratio_dial, &QDial::valueChanged, compressor, &CompressorController::set_ratio);
  return effect;
}

std::pair<SaturatorController *, QWidget *> Win::new_saturator()
{
  std::pair<SaturatorController *, QWidget *> effect = new_effect<SaturatorController>("saturator.ui");
  SaturatorController *saturator = effect.first;
  QWidget *widget = effect.second;

  QDial *threshold_dial = widget->findChild<QDial *>("threshold_dial");
  QDial *ratio_dial = widget->findChild<QDial *>("ratio_dial");
  connect(threshold_dial, &QDial::valueChanged, saturator, &SaturatorController::set_threshold);
  connect(ratio_dial, &QDial::valueChanged, saturator, &SaturatorController::set_ratio);
  return effect;
}

MainClientController *Win::new_main_client()
{
  MainClientController *main_client = new_controller<MainClientController>();


  create_master_slider(main_client);
  connect(main_client, &MainClientController::new_output_device, this, &Win::update_devices);
  connect(m_audio_devices, SIGNAL(currentIndexChanged(int)), main_client, SLOT(set_output(int)));
  return main_client;
}

QWidget *Win::new_mixer_entry(const char *name)
{
  QWidget *widget = load_ui_file<QWidget *>("track.ui", findChild<QWidget *>("tracks_scroll_area"));
  widget->findChild<QLabel *>("name")->setText(tr(name));

  widget->setFixedWidth(widget->width());
  m_tracks_layout->insertWidget(m_tracks_layout->count() - 1, widget);
  return widget;
}

SynthController *Win::new_synth()
{
  SynthController *synth = new_controller<SynthController>();
  return synth;
}

const char *Win::new_track()
{
  const char *track_id = m_main_client_controller->create_track();

  QWidget *widget = new_mixer_entry(track_id);
  Slider *volume_slider = widget->findChild<Slider *>("volume_slider");
  connect(volume_slider, &Slider::valueChanged, m_main_client_controller, [=, this](int x){
    m_main_client_controller->set_track_volume(track_id, x);
  });
  m_main_client_controller->set_track_volume(track_id, volume_slider->value());
  return track_id;
}

void Win::create_master_slider(MainClientController *main_client)
{
  if(main_client == nullptr)
  {
    if(m_main_client_controller != nullptr)
      main_client = m_main_client_controller;
    else
    {
      fprintf(stderr, "Main client has not been created yet\n");
      return;
    }
  }
  QWidget *widget = new_mixer_entry("master");
  Slider *volume_slider = widget->findChild<Slider *>("volume_slider");
  connect(volume_slider, &Slider::valueChanged, main_client, &MainClientController::set_master_volume);
  main_client->set_master_volume(volume_slider->value());
}

void Win::update_devices()
{
  QString selection = m_audio_devices->currentText();
  m_audio_devices->clear();
  std::vector<const Output*> outputs = m_main_client_controller->get_outputs();
  for(const Output *i : outputs)
    m_audio_devices->addItem(QString::fromStdString(i->name));
  int new_selection = m_audio_devices->findText(selection);
  m_audio_devices->setCurrentIndex(new_selection < 0 ? 0 : new_selection);
}

void Win::setup_connections()
{
  QTimer::singleShot(20, this, [this] () {
    postToThread([this] {
      for(struct Track track : m_tracks)
      {
        if(track.effects.empty())
          m_main_client_controller->m_main_client->connect_to_sink(track.source->get_node_id(), track.track_id);
        for(unsigned i=0; i < track.effects.size(); i++)
        {
          if(i == 0)
            m_main_client_controller->m_main_client->link_nodes(track.source->get_node_id(), track.effects[i]->get_node_id());
          if(i == track.effects.size() - 1)
            m_main_client_controller->m_main_client->connect_to_sink(track.effects[i]->get_node_id(), track.track_id);
          else
            m_main_client_controller->m_main_client->link_nodes(track.effects[i]->get_node_id(), track.effects[i+1]->get_node_id());
        }
      }
      m_main_client_controller->m_main_client->connect_oscilloscope();
    }, m_main_client_controller->thread());
  });
}

void Win::update_scope(std::shared_ptr<float[]> data, int size)
{
  float *timedata=new float[size];
  for(int t=0; t<size; t++)
    timedata[t]=(float)t/size;
  curve->setRawSamples(timedata, data.get(), size);
  oscilloscope->replot();
  delete[] timedata;
}

void Win::set_length(int x)
{
  QString length_text = QString("%1:%2").arg(QString::number(x/60)).arg(QString::number(x%60), 2, '0');
  myWidget->findChild<QLabel *>("end_time")->setText(length_text);
}

void Win::closeEvent(QCloseEvent *event)
{
  bool can_close = true;
  for(Track track : m_tracks)
  {
    if(track.source->is_running())
    {
      can_close = false;
      track.source->window_closed();
    }
  }
  if(!can_close)
    event->ignore();
}

static int keyboard[] = {Qt::Key_Z, Qt::Key_S, Qt::Key_X, Qt::Key_D, Qt::Key_C,
  Qt::Key_V, Qt::Key_G, Qt::Key_B, Qt::Key_H, Qt::Key_N, Qt::Key_J,
  Qt::Key_M, Qt::Key_Comma, Qt::Key_L, Qt::Key_Period,
  Qt::Key_Semicolon, Qt::Key_Slash, 0
};

void Win::keyPressEvent(QKeyEvent *event)
{
  if(event->isAutoRepeat())
    return;
  int key = std::find(keyboard, &keyboard[sizeof(keyboard)/sizeof(int)-1], event->key())-keyboard;
  if(key == sizeof(keyboard)/sizeof(int)-1)
    return;

  // TODO
  ((SynthController *)m_tracks[0].source)->key_pressed(key);
}

void Win::keyReleaseEvent(QKeyEvent *event)
{
  if(event->isAutoRepeat())
    return;
  int key = std::find(keyboard, &keyboard[sizeof(keyboard)/sizeof(int)-1], event->key())-keyboard;

  // TODO
  ((SynthController *)m_tracks[0].source)->key_released(key);
}
