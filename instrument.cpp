#include "instrument.h"
#include "defs.h"
#include <cmath>
#include <vector>

ADSR::ADSR(seconds attack, seconds decay, amplitude sustain, seconds release)
{
  this->start = 1;
  this->attack = attack;
  this->decay = decay;
  this->sustain = sustain;
  this->release = release;
}

Instrument::Instrument(ADSR &desc, mod_const k, osc_t arg_osc)
    : desc(&desc), k(k)
{
  std::vector<osc_t> osc;
  osc.push_back(arg_osc);
  this->osc = osc;
}

Instrument::Instrument(ADSR &desc, mod_const k, std::vector<osc_t> arg_osc, std::vector<semitones> arg_foff)
    : desc(&desc), k(k), osc(arg_osc), foff(arg_foff)
{
}

void Instrument::for_every_oscillator(std::function<void (osc_t, semitones)> callback)
{
  for(unsigned int i=0; i<osc.size(); i++)
  {
    callback(osc[i], foff[i]);
  }
}

float Instrument::curve(float t, seconds len)
{
  float res;

  if(t > len + desc->release)
    return 0;

  if (t <= desc->attack) {
    res = (t / desc->attack) * desc->start;
  } else if (t <= desc->attack + desc->decay) {
    float time_in_decay = t - desc->attack;
    float decayed_amplitude = desc->start - desc->sustain;
    res = desc->start-(time_in_decay*(decayed_amplitude)/desc->decay);
    /*res =
        (time_in_decay / (cos((time_in_decay * M_PI) / (2 * decay)) + decay)) *
            decayed_amplitude +
        desc->start;*/
    // p = ((x -
    // attackTime)/(np.cos(((x-attackTime)*np.pi)/(2*decayTime))+decayTime)) *
    // (sustainAmplitude - startAmplitude)+startAmplitude
  } else if (t <= len) {
    res = desc->sustain;
  }

  if(len < t && t <= len + desc->release) {
    res = curve(len, len)*(desc->release-t+len)/desc->release;
  }
  return res;
}

seconds Instrument::get_release(void)
{
  return desc->release;
}
