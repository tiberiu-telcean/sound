#pragma once
#include "defs.h"
#include <vector>
#include <functional>

class ADSR {
  public:
  amplitude start;
  seconds attack;
  seconds decay;
  amplitude sustain;
  seconds release;
  ADSR(seconds attack, seconds decay, amplitude sustain, seconds release);
};

class Instrument {
  public:
  Instrument(ADSR &desc, mod_const k=.5, osc_t osc=sin);
  Instrument(ADSR &desc, mod_const k=.5, std::vector<osc_t> osc={sin}, std::vector<semitones> foff={0});
  float curve(float, seconds);
  seconds get_release(void);
  double get_k(void) { return k; }
  void for_every_oscillator(std::function<void (osc_t, semitones)>);
  private:
  ADSR *desc;
  double k;
  std::vector<osc_t> osc;
  std::vector<semitones> foff;
};
