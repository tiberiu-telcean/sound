#include <pipewire/pipewire.h>
#include "client.h"
#include "defs.h"
#include "main.h"
#include "pipewire/filter.h"

Client::Client()
  :m_loop(pw_main_loop_new(NULL)),
  m_context(pw_context_new(
    pw_main_loop_get_loop(m_loop),
    pw_properties_new(
      PW_KEY_CONFIG_NAME, "client-rt.conf",
      NULL), 0)),
  m_core(pw_context_connect(m_context, NULL, 0))
{

  pw_loop_add_signal(pw_main_loop_get_loop(m_loop), SIGINT, pw_do_quit, this);
  pw_loop_add_signal(pw_main_loop_get_loop(m_loop), SIGTERM, pw_do_quit, this);
  m_update_event = pw_loop_add_idle(pw_main_loop_get_loop(m_loop), false, pw_update, this);

  m_builder = SPA_POD_BUILDER_INIT(m_buffer, sizeof(m_buffer));
  m_audio_info = SPA_AUDIO_INFO_RAW_INIT(
    .format = SPA_AUDIO_FORMAT_F32,
    .flags = 0,
    .rate = (uint32_t)sample_rate,
    .channels = 1,
    .position = {});
  m_params[0] = spa_format_audio_raw_build(
    &m_builder,
    SPA_PARAM_EnumFormat,
    &m_audio_info);

}

Client::~Client()
{
  spa_hook_remove(&m_event_listener);
  pw_context_destroy(m_context);
  pw_main_loop_destroy(m_loop);
}

void Client::update()
{
  QCoreApplication::processEvents();
  pw_loop_enable_idle(pw_main_loop_get_loop(m_loop), m_update_event, false);
}

void Client::async_update()
{
  pw_loop_enable_idle(pw_main_loop_get_loop(m_loop), m_update_event, true);
}

Source::Source(const char* name) : Filter(name)
{
  m_output_port = static_cast<struct port *>(pw_filter_add_port(m_filter,
      PW_DIRECTION_OUTPUT,
      PW_FILTER_PORT_FLAG_MAP_BUFFERS,
      sizeof(struct port),
      pw_properties_new(
        PW_KEY_FORMAT_DSP, "32 bit float mono audio",
        PW_KEY_PORT_NAME, "output",
        NULL),
      NULL, 0));

  if (pw_filter_connect(m_filter,
        PW_FILTER_FLAG_RT_PROCESS,
        NULL, 0) < 0) {
    fprintf(stderr, "can't connect\n");
    return;
  }
}

Source::~Source()
{
  pw_filter_destroy(m_filter);
}

Filter::Filter(const char* name) :Client(), m_name(name)
{
  m_filter_events = {
    .version = PW_VERSION_FILTER_EVENTS,
    .destroy = nullptr,
    .state_changed = nullptr,
    .io_changed = nullptr,
    .param_changed = nullptr,
    .add_buffer = nullptr,
    .remove_buffer = nullptr,
    .process = pw_on_process,
    .drained = nullptr,
    .command = nullptr,
  };

  m_filter = pw_filter_new(m_core, m_name,
      pw_properties_new(
        PW_KEY_MEDIA_TYPE, "Audio",
        PW_KEY_MEDIA_CATEGORY, "Filter",
        PW_KEY_MEDIA_ROLE, "DSP",
        PW_KEY_NODE_ALWAYS_PROCESS, "true",
        PW_KEY_NODE_NAME, m_name,
        PW_KEY_NODE_AUTOCONNECT, "true",
        NULL));

  pw_filter_add_listener(m_filter, &m_event_listener, &m_filter_events, this);
}

Filter::~Filter()
{
  pw_filter_destroy(m_filter);
}
