import numpy as np
import wave as wv
from os import system
from functools import cache
#from typing import List
from defs import *
import matplotlib.pyplot as plt
vol=[1,1]
def st(n:Semitones) ->Hz:
    if(n==0):
        return A
    return A*(2**(1/12))**n

def ft(x:Time, k:ModConst, fm:Frequency) ->Pulse:
    return k*np.sin(w(fm)*x)/5
@cache
def w(x:Time) ->AngVel:
    return x*2*np.pi
def wave(v:Lenght, freq:Hz, time:Seconds =1, volume:Volume =1, ins=Instrument(Envelope(), [])):
    global vol
    e=ins.envelope
    modulations=ins.modulations
    fm = 5
    base=np.array([i for i in np.arange(0.0, time, 1/sampleRate, dtype='<f4')])
    mod=[i*w(freq)+ft(i, k, fm) for i in base];
    #base=np.divide(base,sampleRate)
    curve=e.curve
    for i in np.arange(0, time*sampleRate):
        vol_modulation=curve(i/sampleRate, time)
        #wav_modulation=sin(i*(50*2*np.pi))
        #for i in modulations:
        #    mod=np.add(mod, i.gen(base, freq))
        #mod=np.divide(mod, len(modulations)+1)
        wav:Pulse=np.arcsin(np.sin((base[int(i)]*w(freq)+ft(base[int(i)],1,fm))))
        yield np.multiply(vol[v], wav)*vol_modulation
    #return np.array(np.multiply(vol[v], wav)*vol_modulation, dtype='<f4');

def pause(time:Seconds=1) ->Wave:
    return np.array([0 for _ in range(int(time*sampleRate))], dtype='<f4');

def note(v:Lenght, s:Semitones, l:Lenght, a:int, i:Instrument) ->Wave:
    l=(60/bpm)/l
    cutoff=.5
    if(s==P):
        return pause(l)
    if(a>0):
        return np.concatenate([list(wave(v, st(s), l*(cutoff), 1)), pause(l*(1-cutoff))])
    return wave(v, st(s), l, 1, i)

def save(name: Path, content: Wave):
    #with wv.open(name, "wb") as out:
    #    out.setnchannels(1)
    #    out.setsampwidth(4)
    #    out.setframerate(sampleRate)
    #    out.writeframes(bytes(content))
    #base=[i for i in np.arange(0.0, time, 1/sampleRate, dtype='<f4')];
    #plt.plot(np.sin(base*w(20)+ft(i,1,5)))
    with open(name, "wb") as out:
        out.write(bytes(content))

def play(name: Path):
    system("bin/ffplay -f f32le -showmode 1 -ar %f %s"%(sampleRate, name))

output:Path = "output.bin"
duration:Seconds = 1
seql=[]
def load(name:Path):
    global seql
    global bpm
    global osc
    global ins
    seql=[list(),list()]
    seq=seql[0]
    seqt=seql[1]
    with open(name, 'r') as inp:
        x=inp.readline()
        while x!='T\n':
            if(x.split(' ')[0]=='S'):
                try:
                    bpm=int(x.split(' ')[1])
                    osc=eval(x.split(' ')[2])
                    ins=eval(x.split(' ')[3])
                except IndexError:
                    pass
                x=inp.readline()
                continue
            if (x==''):
                break
            if(',' in x):
                a=[]
                for i in x.split(','):
                    a+=[list(map(float, [j for j in i.split(' ') if j!='' and j!='\n']))]
                seq+=[a]
            else:
                seq+=[list(map(float, [i for i in x.split(' ') if i!='' and i!='\n']))]
            x=inp.readline()
        else:
            x=inp.readline()
            while x!='':
                if(',' in x):
                    a=[]
                    for i in x.split(','):
                        a+=[list(map(float, [j for j in i.split(' ') if j!='' and j!='\n']))]
                    seqt+=[a]
                else:
                    seqt+=[list(map(float, [i for i in x.split(' ') if i!='' and i!='\n']))]
                x=inp.readline()

def track(v, bar=0, ins:Instrument =Instrument(Envelope(),[])):
    a=np.array([])
    seq=seql[v]
    for i in seq:
        if(hasattr(i[0], '__iter__')):
            j=i[0]
            b=np.multiply(list(note(v, j[0], j[1], j[2], ins)), 1/len(i))
            for j in i:
                b=np.add(b, np.multiply(list(note(v, j[0], j[1], j[2], ins)), 1/len(i)))
            a=np.concatenate([a,b])
        else:
            a=np.concatenate([a, list(note(v, i[0], i[1], i[2], ins))])
        if(bar!=0):
            bar.next()
    if(bar!=0):
        bar.finish()
    return np.multiply(a, vol[v], dtype='<f4')

if(__name__=="__main__"):
    load("sotw.txt")
    final:Wave = np.concatenate([note(v, i[0], i[1], i[2]) for i in seq])
    save(output, final)
    play(output)
