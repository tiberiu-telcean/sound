import numpy as np
from functools import cache
import random

# Types ------

Amplitude = float
Pulse = Amplitude
Frame = list[Pulse]
Wave = list[Pulse]
Path = str
Hz = float
Seconds = float
Time = Seconds
Frequency = Hz
Period = list[Time]
Semitones = int
Lenght = int
Volume = float
AngVel = float
ModConst = float

sampleRate:Hz = 48000.0
frameSize:Lenght = 1024
hopSize:Lenght = 512
A:Hz= 440.0
P:Semitones=500
bpm:Lenght=120
threshold=.4
ratio=5
delay:Seconds=.0001
k:ModConst = .1
rho=1
λ=.4
ω:AngVel=2*np.pi

# Converters ------

def block(a:Wave):
    for i in range(0, a.size-hopSize, hopSize):
        yield a[i:i+hopSize]

def blockhop(a:Wave):
    for i in range(0, a.size-hopSize, hopSize):
        yield a[i:i+frameSize]

# Oscillators ------

def stable(x,l):
    if(hasattr(x, '__iter__')):
        try:
            return [l(i) for i in x]
        except:
            return [l(i,i) for (t,i) in enumerate(x)]
    else:
        return l(x, x)
@cache
def sin(x, f):
    return np.sin(x*ω*f)
asin=np.arcsin
def squ(x, f=1):
    def r(x):
        if (x<=0):
            return -1
        else:
            return 1
    return stable(x, lambda x:r(sin(x)))
def saw(x, f=1):
    return stable(x, lambda x:2*(np.mod(x,2*np.pi)/(2*np.pi)-.5))
def noi(x, f=1):
    ω=2*np.pi
    return stable(x, lambda x,t:sin(x, (f-x/10))+(random.random()-.5)*.20*np.cos(t)*(t<(.005*sampleRate)))
@cache
def tri(x, f=1):
    return asin(sin(x, f))
osc=tri

# Classes ------

class Envelope:
    def __init__(self, a=.05, d=.3, s=.2, r=.05):
        self.attackTime = a
        self.decayTime = d
        self.startAmplitude = 1.0
        self.sustainAmplitude = s
        self.releaseTime = r
    @cache
    def curve(self, x:Seconds, t:Seconds) ->Volume:
        p=0
        attackTime= self.attackTime
        decayTime = self.decayTime
        releaseTime=self.releaseTime
        startAmplitude=self.startAmplitude
        sustainAmplitude=self.sustainAmplitude
        if((attackTime+decayTime+releaseTime)>t):
            decayTime=t-(attackTime+releaseTime)
        if(x<=attackTime):
            p = (x/attackTime)*startAmplitude
        if(x> attackTime and x<=(attackTime+decayTime)):
        #    p=((x - attackTime)/(np.cos(((x-attackTime)*np.pi)/(2*decayTime))+decayTime)) * (sustainAmplitude - startAmplitude)+startAmplitude
            p=((startAmplitude-sustainAmplitude)*(x-attackTime-decayTime)**2)/(decayTime)**2+sustainAmplitude
        if(x>(attackTime+decayTime) and x<=(t-releaseTime)):
            p = sustainAmplitude
        if(x>(t-releaseTime)):
            p = sustainAmplitude-sustainAmplitude*((x-(t-releaseTime))/releaseTime)
        return p;

class Modulation:
    def __init__(self, a, b, o=sin):
        self.amp=a
        self.freq=b
        self.osc=o
    def gen(self, base:Wave, freq:Hz) ->Wave:
        w = lambda x:x*2*np.pi
        return [ self.amp*self.osc(i*w(freq*self.freq)) for i in base]

class Instrument:
    def __init__(self,e:Envelope = Envelope(),m:list[Modulation] =[]):
        self.envelope:Envelope=e
        self.modulations:list[Modulation]=m

ins:Instrument = Instrument(Envelope(), [])
