from defs import *
import numpy as np
class Effect:
    def __init__(self):
        self.r = lambda x, wave: wave[x]
        self.ar = lambda x:1/((x+1)**2)
    def gen(self, wave:Wave, t:Lenght):
        time=t/sampleRate
        mod=lambda i:int(max(time-i/sampleRate*delay, 0)*sampleRate)
        return np.sum([self.r(mod(i), wave)*self.ar(i) for i in range(1,rho)])
def combine(wave:Wave, a:Effect):
    return [j+a.gen(wave, i) for (i,j) in enumerate(wave)]

