# File: main.py
import sys
import json
import main
import importlib
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import QApplication
from PySide6.QtWidgets import QPushButton, QLabel, QProgressBar, QComboBox, QCheckBox, QDial, QSlider
from PySide6.QtGui import QPixmap
from PySide6.QtCore import QFile, QIODevice, Slot
from PySide6.QtCore import Qt, QLine
from PySide6.QtCore import QObject, QThread
from PySide6.QtCore import Signal

class Worker(QObject):
    finished = Signal()
    def __init__(self,prog):
        super().__init__()
        self.prog=prog
    def run(self):
        main.main(self.prog)
        self.finished.emit()
class Win:
    def __init__(self,app):
        ui_file_name = "main.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print("Cannot open {}: {}".format(ui_file_name, ui_file.errorString()))
            sys.exit(-1)
        self.loader = QUiLoader()
        self.window = self.loader.load(ui_file)
        ui_file.close()
        if not self.window:
            print(self.loader.errorString())
            sys.exit(-1)
        self.window.show()
        self.btn = self.window.player.findChild(QPushButton, 'play')
        self.btn.clicked.connect(self.inc)
        self.one = self.window.mixer.findChild(QSlider, 'track1_volume')
        self.one.valueChanged.connect(self.v)
        self.two = self.window.mixer.findChild(QSlider, 'track2_volume')
        self.two.valueChanged.connect(self.vt)
        self.prog = self.window.player.findChild(QProgressBar, 'play_bar')
    @Slot()
    def v(self):
        main.wav.vol[0]=self.one.value()/100
    @Slot()
    def vt(self):
        main.wav.vol[1]=self.two.value()/100
    @Slot()
    def inc(self):
        importlib.reload(main)
        self.thread=QThread()
        self.worker=Worker(self.prog)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.thread.start()
if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("Breeze")
    win=Win(app)
    sys.exit(app.exec_())
