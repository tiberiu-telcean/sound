import wav
import numpy as np
import sys
from defs import *
from progress.bar import Bar
import reverb
import compressor
class P:
    disabled=False
    value=0
    def setValue(*args):
        pass
class B:
    def __init__(self,prog,m):
        #self.bar=bar
        self.prog=prog
        self.v=0
        self.m=m
    def next(self):
        self.v=self.v+1
        self.prog.disabled=False
        self.prog.setValue(self.v/self.m*100)
    def finish(self):
        self.v=0
        self.prog.setValue(self.v/self.m*100)
        self.prog.disabled=True
def main(prog=P()):
    wav.ins = Instrument(Envelope(), [Modulation(.5,2,tri), Modulation(.1,0,noi)])
    if(len(sys.argv)>1):
        wav.load(sys.argv[1])
    else:
        wav.load("sotw.txt")
    output:wav.Path = "output.bin"
    tracks=list()
    for (i,j) in enumerate(wav.seql):
        #bar = Bar("Track %d"%(i), max=len(j) )
        bar = B(prog, len(j))
        tracks+=[np.multiply(wav.track(i, bar, ins), .5)]
    fir=tracks[0]
    sec=tracks[1]
    if(len(fir)>len(sec)):
        sec=np.array(np.pad(sec, (0, len(fir)-len(sec)), 'constant', constant_values=0), dtype='<f4')
    else:
        fir=np.array(np.pad(fir, (0, len(sec)-len(fir)), 'constant', constant_values=0), dtype='<f4')
    final=np.add(fir,sec)
    #for i in np.arange(0, delay, delay/3):
    #    temp=[0 for _ in np.arange(0, i, 1/sampleRate)]
    #    final=np.add(np.concatenate([final, temp]), np.concatenate([temp, np.multiply(final, i/delay)]))
    #effects=[{"name":reverb}, {"name":compressor}]
    #effects=[{"name":reverb}]
    effects=[{"name":compressor}]
    #effects=[]
    for i in effects:
        final=i["name"].combine(final, i["name"].Effect())
    final=np.array(final, dtype='<f4')
    wav.save(output, final)
    wav.play(output)
if(__name__=="__main__"):
    import cProfile
    import pstats
    with cProfile.Profile() as pr:
        main()
    stats = pstats.Stats(pr)
    stats.sort_stats(pstats.SortKey.TIME)
    stats.dump_stats(filename='main.prof')
    
