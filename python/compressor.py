from defs import *
import numpy as np
import scipy as sp
import scipy.integrate
#import matplotlib.pyplot as plt
from functools import cache
class Effect:
    def __init__(self):
        self.ta:Seconds=-1
        self.att:Seconds=.003
        pass
    def env(self, a:Wave) ->Envelope:
        b=[]
        for i in blockhop(a):
            b.append(max(i))
        return b
    @cache
    def r(self, x:Pulse):
        @cache
        def f(a:Amplitude):
            if(a>threshold):
                return a*(threshold+(a-threshold)/ratio)
            #elif(b<-t):
            #    return a*min((-t+(b+t)/ratio)/b,1)
            else:
                return a
        return sp.integrate.quad(f, x-λ, x+λ)[0]/(2*λ)
    def n(self, x:Frame, e:Amplitude):
        for i in x:
            if e==0:
                yield 0
                continue
            yield i/e*self.r(e)
    def gen(self, wave:Wave):
        global threshold
        a=self.env(wave)
        t=threshold
        #plt.plot([j*hopSize for (j,i) in enumerate(a)], a)
        #plt.plot(wave)
        #plt.show()
        for (i,j) in enumerate(block(wave)):
            yield list(self.n(j, a[i]))
def combine(wave:Wave, a:Effect):
    return np.concatenate(np.array(list(a.gen(wave))))

