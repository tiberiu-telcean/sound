#include <QObject>
#include <QWidget>
#include <QMainWindow>
#include <QCloseEvent>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QUiLoader>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <memory>
#include <qwt/QwtPlot>
#include <qwt/QwtPlotCurve>

#include "defs.h"
#include "dB.h"

class EffectController;
class MainClientController;
class SynthController;
class CompressorController;
class SaturatorController;
class Controller;

class Loader : public QUiLoader
{
public:
  QWidget *createWidget(const QString &className, QWidget *parent = nullptr, const QString &name = QString()) override;
};

struct Track {
private:
  QVBoxLayout *layout;
  QGroupBox *ui_group;
public:
  Controller *source;
  std::vector<EffectController *> effects;
  const char *track_id;

  Track(Controller *controller, const char *name, QVBoxLayout *effects_layout);
  void add_effect(std::pair<EffectController *, QWidget *> effect);
};

class Win : public QMainWindow
{
  Q_OBJECT
  Loader m_loader;
  QWidget *myWidget=NULL;

  QVBoxLayout *m_effects_layout;
  QHBoxLayout *m_tracks_layout;
  QComboBox *m_audio_devices;
  QWidget *m_mixer;
  QPushButton *m_play_button;
  QwtPlot *oscilloscope;
  QwtPlotCurve *curve;
  QComboBox *instrument;

  std::vector<struct Track> m_tracks;
  MainClientController *m_main_client_controller;
  template<typename T>
  T load_ui_file(const char *file, QWidget *parent = nullptr);
  void setup_connections();

  template<typename T>
  T *new_controller();
  template<typename T>
  std::pair<T *, QWidget *> new_effect(const char *ui_file);
  std::pair<CompressorController *, QWidget *> new_compressor();
  std::pair<SaturatorController *, QWidget *> new_saturator();
  MainClientController *new_main_client();
  QWidget *new_mixer_entry(const char *name="");
  SynthController *new_synth();
  const char *new_track();

  void create_master_slider(MainClientController *main_client = nullptr);
public slots:
  void update_devices();
public:
  explicit Win(QWidget *parent = nullptr);
  void update_scope(std::shared_ptr<float[]> data, int size);
  void progress(int x) { myWidget->findChild<QSlider *>("play_bar")->setValue(x); }
  void set_length(int x);
  int getInstrument() const { return instrument->currentIndex(); }
protected:
  void closeEvent(QCloseEvent *event) override;
  void keyPressEvent(QKeyEvent *event) override;
  void keyReleaseEvent(QKeyEvent *event) override;
};
