#pragma once
#include <cmath>

double pools(double t);
double sawtooth(double t);
double square(double t);
double wnoise(double t);
double tri(double t);
double steps(double t);
