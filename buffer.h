#pragma once
#include <cassert>
#include <cstring>
#include <functional>

template<typename T>
class Buffer {
private:
  unsigned int m_size;
  unsigned int m_end;
  T *m_buffer_1;
  T *m_buffer_2;
  T **m_current_buffer;

  void switch_buffers()
  {
    if(m_current_buffer == &m_buffer_1)
      m_current_buffer = &m_buffer_2;
    else
      m_current_buffer = &m_buffer_1;
  }

  T *get_opposite_buffer() const
  {
    if(m_current_buffer == &m_buffer_1)
      return m_buffer_2;
    else
      return m_buffer_1;
  }
public:
  Buffer(unsigned int size)
    :m_size(size),
    m_end(0)
  {
    m_buffer_1 = new T[size]();
    m_buffer_2 = new T[size]();
    m_current_buffer = &m_buffer_1;
  }

  bool can_add(unsigned int size) const { return m_end + size <= m_size; }

  unsigned int copy_and_remove_from_beginning(T *dest, unsigned int size)
  {
    if(size > m_end)
      size = m_end;
    memcpy(dest, *m_current_buffer, size*sizeof(T));
    memcpy(get_opposite_buffer(), *m_current_buffer+size, (m_end-size)*sizeof(T));
    switch_buffers();
    m_end -= size;
    return size;
  }
  void concatenate_to_end(const T *src, unsigned int size)
  {
    assert(can_add(size));
    memcpy(*m_current_buffer+m_end, src, size*sizeof(T));
    m_end += size;
  }
  void fill(std::function <bool (Buffer *)> callback) { while(callback(this)); }
};
