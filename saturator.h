#pragma once
#include <QMutex>

#include "defs.h"
#include "effect.h"

class SaturatorController;

class Saturator : public Effect
{
public:
  Saturator(SaturatorController *controller) :Effect("saturator"), m_controller(controller) {}
  void on_process(struct spa_io_position *) override;

  void set_ratio(float);
  void set_threshold(float);
private:
  factor m_ratio = 5;
  amplitude m_threshold = .4;
  QMutex m_parameter_lock;
  SaturatorController *m_controller;
};
