#pragma once
#include <pipewire/pipewire.h>
#include <QMutex>
#include <string>
#include <vector>
#include "buffer.h"
#include "client.h"

struct Port
{
  uint32_t node_id;
  uint32_t port_id;
  pw_direction direction;
  const char *name;
};

struct Output
{
  uint32_t node_id;
  std::string name;
  Output(uint32_t arg_node_id) :node_id(arg_node_id) {};
};

struct Link
{
  struct pw_link *link;
  struct pw_properties *props;
  struct spa_hook *event_listener;
  uint32_t input_port_id;
  uint32_t output_port_id;
  uint32_t link_id = 0;
  Link(struct pw_core *core, struct pw_properties *props, uint32_t input_id, uint32_t output_id) :link((struct pw_link *)pw_core_create_object(core, "link-factory", PW_TYPE_INTERFACE_Link, PW_VERSION_LINK, &props->dict, 0)), props(props), event_listener(new spa_hook()), input_port_id(input_id), output_port_id(output_id) {};
  ~Link()
  {
    delete event_listener;
    pw_properties_free(props);
  }
};

class MainClientController;

class MainClient : public Client
{
  MainClientController *m_controller;

  struct pw_registry *m_registry;
  QMutex m_registry_lock;

  struct pw_registry_events m_registry_events = {
    .version = PW_VERSION_REGISTRY_EVENTS,
    .global = MainClient::pw_registry_event_global,
    .global_remove = nullptr
  };
  struct pw_link_events m_output_link_events = {
    .version = PW_VERSION_LINK_EVENTS,
    .info = MainClient::pw_output_link_info
  };
  struct pw_filter_events m_sink_events = {
    .version = PW_VERSION_FILTER_EVENTS,
    .destroy = nullptr,
    .state_changed = nullptr,
    .io_changed = nullptr,
    .param_changed = nullptr,
    .add_buffer = nullptr,
    .remove_buffer = nullptr,
    .process = MainClient::pw_sink_process,
    .drained = nullptr,
    .command = nullptr,
  };
  struct pw_filter_events m_source_events = {
    .version = PW_VERSION_FILTER_EVENTS,
    .destroy = nullptr,
    .state_changed = nullptr,
    .io_changed = nullptr,
    .param_changed = nullptr,
    .add_buffer = nullptr,
    .remove_buffer = nullptr,
    .process = MainClient::pw_source_process,
    .drained = nullptr,
    .command = nullptr,
  };
  struct pw_filter_events m_oscilloscope_events = {
    .version = PW_VERSION_FILTER_EVENTS,
    .destroy = nullptr,
    .state_changed = nullptr,
    .io_changed = nullptr,
    .param_changed = nullptr,
    .add_buffer = nullptr,
    .remove_buffer = nullptr,
    .process = MainClient::pw_oscilloscope_process,
    .drained = nullptr,
    .command = nullptr,
  };

  struct Track {
    struct port *port;
    const char *track_id;
    float volume;
  };

  struct pw_filter *m_sink;
  std::vector<struct Track> m_left_sink_ports;
  std::vector<struct Track> m_right_sink_ports;

  struct pw_filter *m_source;
  struct port *m_left_source_port;
  struct port *m_right_source_port;

  struct pw_filter *m_oscilloscope;
  struct port *m_oscilloscope_port;

  std::vector<std::unique_ptr<Output>> m_outputs;
  std::vector<std::unique_ptr<Link>> m_output_links;
  struct spa_hook m_output_link_listener;

  static void pw_registry_event_global(void *userdata, uint32_t id, uint32_t permissions, const char *type, uint32_t version, const struct spa_dict *props) { ((MainClient *)userdata)->registry_event_global(id, permissions, type, version, props); }
  void registry_event_global(uint32_t id, uint32_t permissions, const char *type, uint32_t version, const struct spa_dict *props);
  void sink_process(struct spa_io_position *);
  static void pw_sink_process(void* userdata, struct spa_io_position *position) { ((MainClient *)userdata)->sink_process(position); }
  void source_process(struct spa_io_position *);
  static void pw_source_process(void* userdata, struct spa_io_position *position) { ((MainClient *)userdata)->source_process(position); }
  void oscilloscope_process(struct spa_io_position *);
  static void pw_oscilloscope_process(void* userdata, struct spa_io_position *position) { ((MainClient *)userdata)->oscilloscope_process(position); }
  static void pw_output_link_info(void *userdata, const struct pw_link_info *info) { ((MainClient *)userdata)->output_link_info(info); }
  void output_link_info(const struct pw_link_info *info);

  struct port *create_port(pw_filter *node, pw_direction direction, const char *format, const char *name, bool autoconnect);
  std::unique_ptr<float[]> add_tracks(std::vector<Track> tracks, uint32_t n_samples);

  std::vector<Port> m_ports;
  Buffer<float> *m_left_buffer;
  Buffer<float> *m_right_buffer;
  float m_master_volume = 1;
  QMutex m_lock;
public:
  MainClient(MainClientController *);
  ~MainClient();

  void remove_link(const Link *link);
  std::unique_ptr<Link> link_ports(uint32_t n1, uint32_t p1, uint32_t n2, uint32_t p2);
  std::vector<std::unique_ptr<Link>> link_nodes(uint32_t n1, uint32_t n2, const char *input_prefix = "input", const char *output_prefix = "output");

  void set_output(std::unique_ptr<Output> &output);
  void set_output(int index);
  std::vector<const Output*> get_output_list() const;
  void connect_to_sink(uint32_t node, const char *track_id);
  void connect_oscilloscope();
  const char *create_track();
  void set_track_volume(const char *track_id, float volume);
  void set_master_volume(float volume);
};
