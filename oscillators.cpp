#include <cmath>
#include <cstdlib>
#include "defs.h"
#include "oscillators.h"

double pools(double t) {
  if(fmod(t,4)<=2)
    return sqrt(4-exp2(   fmod( t, 2)))-1;
  else
    return -(sqrt(4-exp2( fmod( t, 2)))-1);
}

double sawtooth(double t) {
  const int n=10;
  double s=0;
  for(int i=1; i<=n; i++) {
    s+=sin(t*i)/i;
  }
  return s/2;
}

double square(double t) {
  const int n=20;
  double s=0;
  for(int i=1; i<=n; i+=2) {
    s+=sin(t*i)/i;
  }
  return s/2;
}

double random_frame[frame_size];

__attribute__ ((constructor)) void gen_wnoise(void)
{
  for(int i=0; i<frame_size; i++)
  {
    random_frame[i]=(float)rand()/(float)RAND_MAX;
  }
}

double wnoise(double t)
{
  return random_frame[(int)(t*sample_rate)%frame_size];
}

double tri(double t) {
  const int n=10;
  double s=0;
  for(int i=1; i<=n; i+=4) {
    s+=sin(t*i)/(i*i);
  }

  for(int i=3; i<=n; i+=4) {
    s-=sin(t*i)/(i*i);
  }
  return s/2;
}

double steps(double t) {
  return sin(t)+sin(t*3)/8.3+sin(t*5)/8.3+sin(t*7)/11;
}
