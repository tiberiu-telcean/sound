#include <algorithm>
#include <spa/control/control.h>

#include "client.h"
#include "compressor.h"
#include "controllers.h"
#include "dB.h"
#include "defs.h"
#include "main.h"
#include "oscillators.h"
#include "synth.h"
#include "wave.h"


ADSR basic(.01, .01, .8, .01);
ADSR flute_env(.01, .05, .8, .02);
ADSR drum_env(.01, .1, 0, 0);
Instrument instruments[] = {
                            {flute_env, 0, {steps, sawtooth}, {0, -12}},
                            {drum_env, .2, {wnoise, square}, {0, -36}},
                           };

Synth::Synth(SynthController *controller) :Source("Synthesizer"), m_try_closing(false), m_volume(.4), m_controller(controller) {
  m_notes = {{2, 0, .5}, {5, .5, .5}, {6, 1, .5}, {2, 1.7, .5}, {5, 2.2, .5}, {8, 2.7, .2}, {7, 2.9, .5}};
  std::sort(m_notes.begin(), m_notes.end());
  note last_note = m_notes[m_notes.size()-1];
  m_instrument = &instruments[m_controller->getInstrument()];
  n_frames = (last_note.start+last_note.length+m_instrument->get_release())*sample_rate;
  if(n_frames % frame_size)
    n_frames += frame_size-n_frames%frame_size;
  n_frames /= frame_size;

  m_controller->set_length((n_frames*frame_size)/sample_rate);
  at_frame=0;
  play_buffer = new Buffer<float>(1024);

  m_midi_in_port = static_cast<struct port *>(pw_filter_add_port(m_filter,
      PW_DIRECTION_INPUT,
      PW_FILTER_PORT_FLAG_MAP_BUFFERS,
      sizeof(struct port),
      pw_properties_new(
              PW_KEY_FORMAT_DSP, "8 bit raw midi",
              PW_KEY_PORT_NAME, "input",
              NULL),
      NULL, 0));
};

int Synth::render_one_frame(int i, Buffer<float> &buffer)
{
  m_controller->progress((int)((float)i/n_frames*100));
  seconds begin_frame=(i*frame_size)/sample_rate;
  seconds end_frame=((i+1)*frame_size)/sample_rate;
  Wave *w = wave::pause_samples(frame_size);
  for(auto note : m_notes)
  {
    seconds audible_length = note.length + m_instrument->get_release();
    seconds end_time = note.start + audible_length;
    if(note.start >= begin_frame && note.start <= end_frame) // beginning in this frame
    {
      Wave *initial_silence = wave::pause(note.start - begin_frame);
      int length_within_frame = frame_size - initial_silence->get_len();
      Wave *p = wave::make_wave(A4 * wave::st(note.pitch), {.offset = 0, .length = length_within_frame, .note_length = note.length}, m_volume, m_instrument);
      initial_silence->add_to_end(p);
      delete p;
      w->add(initial_silence);
      delete initial_silence;
    } else
    if(end_time >= begin_frame && end_time <= end_frame) // ending in this frame
    {
      Wave *final_silence = wave::pause(end_frame - end_time);
      int length_within_frame = frame_size - final_silence->get_len();
      int offset_within_note = i * frame_size - note.start * sample_rate;
      Wave *p = wave::make_wave(A4 * wave::st(note.pitch), {.offset = offset_within_note, .length = length_within_frame, .note_length = note.length}, m_volume, m_instrument);
      p->add_to_end(final_silence);
      delete final_silence;
      w->add(p);
      delete p;
    } else
    if(note.start < begin_frame && end_frame < end_time)
    {
      int offset_within_note = i * frame_size - note.start * sample_rate;
      Wave *p = wave::make_wave(A4 * wave::st(note.pitch), {.offset = offset_within_note, .length = frame_size, .note_length = note.length}, m_volume, m_instrument);
      w->add(p);
      delete p;
    }
  }

  buffer.concatenate_to_end(w->get_arr(), w->get_len());
  delete w;
  return frame_size;
}

void Synth::key_pressed(int pitch)
{
  for(note &i : m_notes)
  {
    if(i.pitch == pitch)
    {
      i.start = at_frame*frame_size/sample_rate;
      i.length = 99999;
      i.should_be_removed = false;
      return;
    }
  }
  m_notes.push_back({pitch, at_frame*frame_size/sample_rate, 99999});
}

void Synth::key_released(int pitch)
{
  for(note &i : m_notes)
  {
    if(i.pitch == pitch)
    {
      i.length = (at_frame)*frame_size/sample_rate-i.start;
      i.should_be_removed = true;
    }
  }
}

void Synth::do_quit(int signal)
{
  pw_main_loop_quit(m_loop);
  m_controller->stop(signal);
}

void Synth::read_midi()
{
	struct pw_buffer *b;
	struct spa_buffer *buf;
	struct spa_data *d;
	struct spa_pod *pod;
	struct spa_pod_control *c;

	b = pw_filter_dequeue_buffer(m_midi_in_port);
	if (b == NULL)
		return;

	buf = b->buffer;
	d = &buf->datas[0];
	if (d->data == NULL)
		return;
	if ((pod = (spa_pod *)spa_pod_from_data(d->data, d->maxsize, d->chunk->offset, d->chunk->size)) == NULL)
		return;
	if (!spa_pod_is_sequence(pod))
		return;
	SPA_POD_SEQUENCE_FOREACH((struct spa_pod_sequence*)pod, c) {
                struct note n;
		if (c->type != SPA_CONTROL_Midi)
			continue;
		// n.track = 0;
		//n.start = ((at_frame) * frame_size + c->offset) / sample_rate;
                n.length = .1;
               // printf("%f\n", n.start);
                //n.pitch = ((uint8_t *)SPA_POD_BODY(&c->value))[1] - 69;
                n.should_be_removed = false;
		// ev.data = SPA_POD_BODY(&c->value),
		// ev.size = SPA_POD_BODY_SIZE(&c->value);
                //m_notes.push_back(n);
	}
	pw_filter_queue_buffer(m_midi_in_port, b);
}

void Synth::on_process(struct spa_io_position *position)
{
  if(m_try_closing) {
    do_quit(0);
    return;
  }
  async_update();

  uint32_t n_samples = position->clock.duration;
  float *buf = (float *)pw_filter_get_dsp_buffer(m_output_port, n_samples);
  if(!buf)
    return;
  // read_midi();

  play_buffer->fill([&](Buffer<float> *b) ->bool{
    if(!b->can_add(frame_size))
      return false;
    render_one_frame(at_frame, *b);
    at_frame++;
    return true;
  });
  unsigned int wrote = play_buffer->copy_and_remove_from_beginning(buf, n_samples);
  fwrite(buf, sizeof(float), wrote, output);
}
