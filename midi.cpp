#include "midi.h"
#include <cstring>

namespace MIDI
{

std::vector<chunk> read_chunks(char *file, size_t size)
{
  size_t index;
  std::vector<chunk> chunks;
  while (index < size) {
    chunk *next_chunk = (chunk *)std::malloc(sizeof(chunk));
    std::memcpy((char *)next_chunk->type, file + index, 4);
    index += 4;
    std::memcpy((uint32_t *)&next_chunk->length, file + index,
                sizeof(uint32_t));
    index += 4;
    size_t chunk_size = be32toh(next_chunk->length);
    char *data = (char *)std::malloc(chunk_size);
    std::memcpy(data, file + index, chunk_size);
    next_chunk->data = data;
    index += chunk_size;
    chunks.push_back(*next_chunk);
    free(next_chunk);
  }
  return chunks;
}

} // namespace MIDI
