#pragma once
#include <cmath>

#include "compressor.h"
#include "controllers.h"
#include "effect.h"
#include "saturator.h"

class EffectController : public Controller
{
  Q_OBJECT
public slots:
  void run() { m_effect->run(); }
  virtual void set_preamp(int preamp) { m_effect->set_preamp((float)preamp / 5); }
  virtual void set_gain(int gain) { m_effect->set_gain((float)gain / 5); }
  virtual void set_mix(int mix) { m_effect->set_mix((float)mix / 100); }
public:
  Effect *m_effect;
  EffectController(Win *window, Effect *effect) :Controller(window), m_effect(effect) {}

  virtual uint32_t get_node_id() const { return m_effect->get_node_id(); }
  const char *get_name() const { return m_effect->get_name(); }
};

class CompressorController : public EffectController
{
  Q_OBJECT
public slots:
  void set_threshold(int x) { ((Compressor *)m_effect)->set_threshold((float) x/100); }
  void set_ratio(int x) { ((Compressor *)m_effect)->set_ratio(std::log2((float) x/8)); }
public:
  Compressor *m_compressor;
  CompressorController(Win *window) :EffectController(window, new Compressor(this)) { m_compressor = static_cast<Compressor *>(m_effect); }
};

class SaturatorController : public EffectController
{
  Q_OBJECT
public slots:
  void set_threshold(int x) { ((Saturator *)m_effect)->set_threshold((float) x/100); }
  void set_ratio(int x) { ((Saturator *)m_effect)->set_ratio(std::log2((float) x/8)); }
public:
  Saturator *m_saturator;
  SaturatorController(Win *window) :EffectController(window, new Saturator(this)) { m_saturator = static_cast<Saturator *>(m_effect); }
};
