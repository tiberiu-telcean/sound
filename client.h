#pragma once

#include <pipewire/pipewire.h>
#include <spa/pod/builder.h>
#include <spa/param/audio/format-utils.h>

struct port {
  struct data* data;
};

class Client {
public:
  struct pw_main_loop *m_loop;
  uint8_t m_buffer[1024];
protected:
  virtual void on_process() {};
  virtual void do_quit(int) {};
  static void pw_on_process(void* userdata) { ((Client *)userdata)->on_process(); }
  static void pw_do_quit(void* userdata, int signal) { ((Client *)userdata)->do_quit(signal); }
  static void pw_update(void * userdata) { ((Client *)userdata)->update(); } 
  void update();
  void async_update();
  struct spa_source *m_update_event;
  bool m_running = false;
  struct spa_pod_builder m_builder;
private:
  struct spa_audio_info_raw m_audio_info;
  struct pw_context *m_context;
protected:
  struct pw_core *m_core;
  struct spa_hook m_event_listener;
  const struct spa_pod *m_params[1];
public:
  Client();
  ~Client();

  bool is_running() const { return m_running; }
  void run() { m_running = true; pw_main_loop_run(m_loop); }
};

class Filter : public Client {
protected:
  struct pw_filter *m_filter;
  struct pw_filter_events m_filter_events;
  const char *m_name;

  using Client::on_process;
  virtual void on_process(struct spa_io_position *) {};
  static void pw_on_process(void* userdata, struct spa_io_position *position) { ((Filter *)userdata)->on_process(position); }
public:
  Filter(const char *name = "audio-filter");
  ~Filter();
  const char *get_name() const { return m_name; }
  virtual uint32_t get_node_id() const { return pw_filter_get_node_id(m_filter); }
};

class Source : public Filter {
protected:
  struct port *m_output_port;

public:
  Source(const char *name = "audio-filter");
  ~Source();
};
