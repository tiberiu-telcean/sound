#include <QThread>

#include "controllers.h"
#include "main.h"
#include "qt.h"

void Controller::stop(int signal)
{
  postToThread([=]{ exit(signal); }, m_window->thread());
}

void MainClientController::update_scope(std::shared_ptr<float[]> data, int size)
{
  postToThread([=, this]{m_window->update_scope(std::move(data), size); }, m_window->thread());
}

void SynthController::progress(int x)
{
  postToThread([=, this]{ m_window->progress(x); }, m_window->thread());
}

void SynthController::set_length(int seconds)
{
  postToThread([=, this]{ m_window->set_length(seconds); }, m_window->thread());
}

int SynthController::getInstrument() const {
  int result = 0;
  postToThread([&]{ result = m_window->getInstrument(); }, m_window->thread());
  return result;
};
