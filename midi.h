#pragma once

#include <cstdint>
#include <cstdlib>
#include <endian.h>
#include <vector>

namespace MIDI
{
struct chunk {
  const char type[4];
  uint32_t length;
  char *data;

  uint32_t get_length(void) const { return be32toh(length); }
};
std::vector<chunk> read_chunks(char *file, size_t size);
} // namespace MIDI
