#include <cstring>
#include <memory>
#include <pipewire/pipewire.h>
#include <QMutexLocker>
#include <QTimer>
#include <spa/utils/dict.h>
#include <spa/node/node.h>

#include "effect.h"
#include "effect_controllers.h"
#include "main_client.h"

MainClient::MainClient(MainClientController *controller) : Client(), m_controller(controller), m_registry(pw_core_get_registry(m_core, PW_VERSION_REGISTRY, 0))
{

  pw_registry_add_listener(m_registry, &m_event_listener, &m_registry_events, this);

  m_sink = pw_filter_new_simple(pw_main_loop_get_loop(m_loop), "Sound sink", pw_properties_new(
        PW_KEY_MEDIA_TYPE, "Audio",
        PW_KEY_MEDIA_CATEGORY, "Sink",
        PW_KEY_MEDIA_ROLE, "Production",
        PW_KEY_NODE_NAME, "Sound sink",
        PW_KEY_NODE_AUTOCONNECT, "false",
        PW_KEY_NODE_ALWAYS_PROCESS, "true",
        NULL), &m_sink_events, this);

  m_source = pw_filter_new_simple(pw_main_loop_get_loop(m_loop), "Sound source", pw_properties_new(
        PW_KEY_MEDIA_TYPE, "Audio",
        PW_KEY_MEDIA_CATEGORY, "Source",
        PW_KEY_MEDIA_ROLE, "Playback",
        PW_KEY_MEDIA_CLASS, "Stream/Output/Audio",
        PW_KEY_NODE_NAME, "Sound source",
        PW_KEY_NODE_AUTOCONNECT, "true",
        NULL), &m_source_events, this);
  m_left_source_port = create_port(m_source, PW_DIRECTION_OUTPUT, "32 bit float mono audio", "output_FL", true);
  m_right_source_port = create_port(m_source, PW_DIRECTION_OUTPUT, "32 bit float mono audio", "output_FR", true);

  m_oscilloscope = pw_filter_new_simple(pw_main_loop_get_loop(m_loop), "Oscilloscope", pw_properties_new(
        PW_KEY_MEDIA_TYPE, "Audio",
        PW_KEY_MEDIA_CATEGORY, "Sink",
        PW_KEY_MEDIA_ROLE, "DSP",
        PW_KEY_NODE_NAME, "Oscilloscope",
        PW_KEY_NODE_AUTOCONNECT, "false",
        PW_KEY_NODE_ALWAYS_PROCESS, "true",
        NULL), &m_oscilloscope_events, this);

  m_oscilloscope_port = create_port(m_oscilloscope, PW_DIRECTION_INPUT, "32 bit float mono audio", "input", false);

  struct spa_process_latency_info latency_info = SPA_PROCESS_LATENCY_INFO_INIT(
      .quantum = 1 / sample_rate,
      .rate = (uint32_t)sample_rate,
      .ns = 10 * SPA_NSEC_PER_MSEC
  );
  m_params[0] = spa_process_latency_build(&m_builder,
			SPA_PARAM_ProcessLatency,
			&latency_info);

  if (pw_filter_connect(m_sink,
        PW_FILTER_FLAG_RT_PROCESS,
        m_params, 1) < 0) {
    fprintf(stderr, "can't connect sink\n");
    return;
  }

  if (pw_filter_connect(m_source,
        PW_FILTER_FLAG_RT_PROCESS,
        m_params, 1) < 0) {
    fprintf(stderr, "can't connect source\n");
    return;
  }

  if (pw_filter_connect(m_oscilloscope,
        PW_FILTER_FLAG_RT_PROCESS,
        m_params, 1) < 0) {
    fprintf(stderr, "can't connect oscilloscope\n");
    return;
  }

  m_right_buffer = new Buffer<float>(2048);
  m_left_buffer = new Buffer<float>(2048);
}

MainClient::~MainClient()
{
  delete m_left_buffer;
  delete m_right_buffer;
  for(std::unique_ptr<Output> &o : m_outputs)
    o.~unique_ptr();
  pw_filter_destroy(m_sink);
  pw_filter_destroy(m_source);
}

void MainClient::sink_process(struct spa_io_position *position)
{
  async_update();
  QMutexLocker l(&m_lock);
  uint32_t n_samples = position->clock.duration;
  if(m_left_buffer->can_add(n_samples))
    m_left_buffer->concatenate_to_end(add_tracks(m_left_sink_ports, n_samples).get(), n_samples);
  if(m_right_buffer->can_add(n_samples))
    m_right_buffer->concatenate_to_end(add_tracks(m_right_sink_ports, n_samples).get(), n_samples);
}

std::unique_ptr<float[]> MainClient::add_tracks(std::vector<Track> tracks, uint32_t n_samples)
{
  auto buffer = std::make_unique<float[]>(n_samples);
  memset(buffer.get(), 0, n_samples*sizeof(float));

  for(Track track : tracks)
  {
    float *dsp_buffer = (float *)pw_filter_get_dsp_buffer(track.port, n_samples);
    if(!dsp_buffer)
      continue;
    for(unsigned i = 0; i < n_samples; i++)
      buffer[i] += dsp_buffer[i] * track.volume * m_master_volume;;
  }
  return buffer;
}

void MainClient::source_process(struct spa_io_position *position)
{
  async_update();
  QMutexLocker l(&m_lock);
  uint32_t n_samples = position->clock.duration;
  float *dsp_left_buffer = (float *)pw_filter_get_dsp_buffer(m_left_source_port, n_samples);
  float *dsp_right_buffer = (float *)pw_filter_get_dsp_buffer(m_right_source_port, n_samples);
  if(dsp_left_buffer)
    m_left_buffer->copy_and_remove_from_beginning(dsp_left_buffer, n_samples);
  if(dsp_right_buffer)
    m_right_buffer->copy_and_remove_from_beginning(dsp_right_buffer, n_samples);
}

void MainClient::oscilloscope_process(struct spa_io_position *position)
{
  QMutexLocker l(&m_lock);
  uint32_t n_samples = position->clock.duration;
  float *dsp_buffer = (float *)pw_filter_get_dsp_buffer(m_oscilloscope_port, n_samples);
  if(!dsp_buffer)
    return;
  std::shared_ptr<float[]> buffer = std::make_shared<float[]>(n_samples);
  memcpy(buffer.get(), dsp_buffer, n_samples * sizeof(float));
  m_controller->update_scope(std::move(buffer), n_samples);
}

void MainClient::registry_event_global(uint32_t id, uint32_t, const char *type, uint32_t, const struct spa_dict *props)
{
  if(std::strcmp(type, PW_TYPE_INTERFACE_Port) == 0) {
    const struct spa_dict_item *item;
    uint32_t node_id, port_id = id;
    pw_direction direction;
    char *name;
    spa_dict_for_each(item, props) {
      if(std::strcmp(item->key, PW_KEY_NODE_ID) == 0)
        sscanf(item->value, "%u", &node_id);
      if(std::strcmp(item->key, PW_KEY_PORT_NAME) == 0)
      {
        name = new char[strlen(item->value)+1];
        std::strcpy(name, item->value);
      }
      if(std::strcmp(item->key, PW_KEY_PORT_DIRECTION) == 0)
      {
        if(std::strcmp(item->value, "in") == 0)
          direction = PW_DIRECTION_INPUT;
        else
          direction = PW_DIRECTION_OUTPUT;
      }
    }
    m_ports.push_back({node_id, port_id, direction, std::move(name)});
  } else if (std::strcmp(type, PW_TYPE_INTERFACE_Node) == 0) {
    const struct spa_dict_item *item;
    std::unique_ptr<Output> output = std::make_unique<Output>(id);
    bool is_sink = false;
    spa_dict_for_each(item, props) {
      if(std::strcmp(item->key, PW_KEY_MEDIA_CLASS) == 0)
        if(std::strcmp(item->value, "Audio/Sink") == 0)
          is_sink = true;
      if(std::strcmp(item->key, PW_KEY_NODE_DESCRIPTION) == 0)
        output->name.append(item->value);
    }
    if(is_sink) {
      m_outputs.push_back(std::move(output));
      m_controller->new_sink();
    }
  }
  async_update();
}

struct port *MainClient::create_port(pw_filter *node, pw_direction direction, const char *format, const char *name, bool autoconnect)
{
  return static_cast<struct port *>(pw_filter_add_port(node,
      direction,
      PW_FILTER_PORT_FLAG_MAP_BUFFERS,
      sizeof(struct port),
      pw_properties_new(
        PW_KEY_FORMAT_DSP, format,
        PW_KEY_PORT_NAME, name,
        PW_KEY_NODE_AUTOCONNECT, autoconnect ? "true" : "false",
        NULL),
      NULL, 0));
}

void MainClient::output_link_info(const struct pw_link_info *info)
{
  if((uint64_t)info < 0x1000)
    return;
  for(std::unique_ptr<Link> &i :  m_output_links)
  {
    if(i->input_port_id == info->input_port_id && i->output_port_id == info->output_port_id)
      i->link_id = info->id;
  }
}

void MainClient::set_output(std::unique_ptr<Output> &output)
{
  if(pw_filter_get_node_id(m_source) == (uint32_t)-1)
  {
    QTimer::singleShot(10, [&] { set_output(output); });
    return;
  }

  for(std::unique_ptr<Link> &i : m_output_links)
    remove_link(i.get());

  m_output_links = link_nodes(pw_filter_get_node_id(m_source), output->node_id, "playback");
  for(std::unique_ptr<Link> &i : m_output_links)
    pw_link_add_listener(i->link, i->event_listener, &m_output_link_events, this);
}

void MainClient::set_output(int index)
{
  set_output(m_outputs[index]);
}

const char *MainClient::create_track()
{
  char *track_id = new char[8];
  sprintf(track_id, "track%2lu", m_left_sink_ports.size()+1);
  char name_left[11];
  char name_right[11];
  sprintf(name_left, "%s_FL", track_id);
  sprintf(name_right, "%s_FR", track_id);
  m_left_sink_ports.push_back({
      .port = create_port(m_sink, PW_DIRECTION_INPUT, "32 bit float mono audio", name_left, false),
      .track_id = track_id,
      .volume = 1});
  m_right_sink_ports.push_back({
      .port = create_port(m_sink, PW_DIRECTION_INPUT, "32 bit float mono audio", name_right, false),
      .track_id = track_id,
      .volume = 1});
  return std::move(track_id);
}

void MainClient::set_track_volume(const char *track_id, float volume)
{
  QMutexLocker l(&m_lock);
  for(struct Track &track : m_left_sink_ports)
    if(strcmp(track_id, track.track_id) == 0)
      track.volume = volume;
  for(struct Track &track : m_right_sink_ports)
    if(strcmp(track_id, track.track_id) == 0)
      track.volume = volume;
}

void MainClient::set_master_volume(float volume)
{
  m_master_volume = volume;
}

std::vector<const Output*> MainClient::get_output_list() const
{
  std::vector<const Output*> result;
  for(const std::unique_ptr<Output> &output : m_outputs)
    result.push_back((const Output *)output.get());
  return result;
}

void MainClient::connect_to_sink(uint32_t node, const char *track_id)
{
  link_nodes(node, pw_filter_get_node_id(m_sink), track_id);
}

void MainClient::connect_oscilloscope()
{
  link_nodes(pw_filter_get_node_id(m_source), pw_filter_get_node_id(m_oscilloscope));
}

void MainClient::remove_link(const Link *link)
{
  QMutexLocker l(&m_registry_lock);
  if(link->link_id)
    pw_registry_destroy(m_registry, link->link_id);
  else
    fprintf(stderr, "cannot remove link\n");
  delete link;
}

std::unique_ptr<Link> MainClient::link_ports(uint32_t n1, uint32_t p1, uint32_t n2, uint32_t p2)
{
  QMutexLocker l(&m_registry_lock);
  char input_node_id[16];
  char input_port_id[16];
  char output_node_id[16];
  char output_port_id[16];
  sprintf(input_node_id, "%u", n1);
  sprintf(input_port_id, "%u", p1);
  sprintf(output_node_id, "%u", n2);
  sprintf(output_port_id, "%u", p2);
  struct pw_properties *props = pw_properties_new(
          PW_KEY_LINK_INPUT_NODE, input_node_id,
          PW_KEY_LINK_INPUT_PORT, input_port_id,
          PW_KEY_LINK_OUTPUT_NODE, output_node_id,
          PW_KEY_LINK_OUTPUT_PORT, output_port_id,
          NULL);
  return std::make_unique<Link>(m_core, props, p1, p2);
}

std::vector<std::unique_ptr<Link>> MainClient::link_nodes(uint32_t n1, uint32_t n2, const char *input_prefix, const char *output_prefix)
{
  std::vector<uint32_t> ports1, ports2;
  std::vector<std::unique_ptr<Link>> result;
  for(struct Port i : m_ports)
  {
    if(i.node_id == n1 && i.direction == PW_DIRECTION_OUTPUT)
      if(!output_prefix || strncmp(i.name, output_prefix, strlen(output_prefix)) == 0)
        ports1.push_back(i.port_id);
    if(i.node_id == n2 && i.direction == PW_DIRECTION_INPUT)
      if(!input_prefix || strncmp(i.name, input_prefix, strlen(input_prefix)) == 0)
        ports2.push_back(i.port_id);
  }
  if(ports1.empty() || ports2.empty())
  {
    fprintf(stderr, "Node has no ports\n");
    return {};
  } else if(ports1.size() == 1)
  {
    for(uint32_t i : ports2)
      result.push_back(link_ports(n2, i, n1, ports1[0]));
  } else
  {
    if(ports2.size() == 1)
    {
      for(uint32_t i : ports1)
        result.push_back(link_ports(n2, ports2[0], n1, i));
    } else if (ports1.size() == ports2.size())
    {
      for(unsigned i = 0; i<ports1.size(); i++)
        result.push_back(link_ports(n2, ports2[i], n1, ports1[i]));
    } else
    {
      fprintf(stderr, "Incompatible number of channels\n");
      return {};
    }
  }
  return result;
}
