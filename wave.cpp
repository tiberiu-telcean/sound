#include "defs.h"
#include "wave.h"
#include "instrument.h"
#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cassert>

int Wave::get_len(void) const { return n_samples; }

void Wave::set_len(int l) {
  del();
  arr=(wave_t)malloc(sizeof(pulse)*l);
  freed=false;
  n_samples=l;
}

void Wave::del(void) {
  if (nofree)
    return;
  if (arr && !freed) {
    free(arr);
    freed=true;
  }
  arr=NULL;
  n_samples = 0;
}

Wave::~Wave(void) {
  del();
  arr=NULL;
}

Wave::Wave(int l) {
  arr=NULL;
  set_len(l);
  nofree = false;
}

Wave::Wave(wave_t a, int l) {
  set_arr(a, l);
  nofree = false;
}

void Wave::set_arr(wave_t a, int l) {
  arr=a;
  n_samples = l;
  freed=false;
}

void Wave::resize(int l) {
  if(freed)
    return;
  arr=(wave_t)std::realloc(get_arr(), sizeof(pulse)*l);
  n_samples = l;
}

void Wave::add(Wave *other) {
  assert(!freed);
  if(other->get_len() > get_len())
    assert(!"TODO");
  for(int i = 0; i < other->get_len(); i++)
    arr[i] += other->arr[i];
}

void Wave::add_to_end(const Wave *other) {
  int initial_length = get_len();
  resize(initial_length + other->get_len());

  for(int i = 0; i < other->get_len(); i++) {
    arr[initial_length + i] = other->arr[i];
  }
}

namespace wave {
  hz st(semitones n) {
    if(n==0)
      return 1;
    return exp2((double)n / 12);
  }
  
  pulse ft(seconds x, mod_const k, frequency fm) {
    return k*sin(ω(fm)*x);
  }

  inline ang_vel ω(frequency x) {
    return M_PI * 2 * x;
  }

  void add(std::vector<Wave> &ws) {
    assert(ws.size() > 0);
    int total_length=0;
    for(Wave i: ws)
    {
      total_length += i.get_len();
    }
    ws[0].resize(total_length);

    for(Wave w2: ws)
    {
      for(int i=0; i < w2.get_len(); i++) {
        ws[0][i] += w2[i];
      }
    }
  }

  /**
   * Make n array of samples with an envelope and FM, from the Instrument class
   * @param freq
   * @param t number and offset of the samples
   * @param volume the samples are multiplied at the end with this
   * @param inst collection of information about producing sound.
   */
  Wave *make_wave(hz freq, wave_region t, volume vol, Instrument *inst) {
    frequency fm = 5;
    int len = t.length;
    Wave *base = new Wave(len);
    double f1;
    seconds note_time;
    for(int i=t.offset; i<len+t.offset; i++) {
      int canon = i-t.offset;
      note_time = i / sample_rate;
      amplitude vol_modulation = inst->curve(note_time, t.note_length);
      f1=ft(note_time, inst->get_k(), fm);
      (*base)[canon] = 0;
      inst->for_every_oscillator([&](osc_t osc, semitones offset) {
        (*base)[canon] += osc(ω(freq*st(offset))*note_time+f1);
      });
      (*base)[canon] = (*base)[canon] * vol * vol_modulation;
    }
    return std::move(base);
  }

  /**
   * Produce an array of 0 values that is t seconds long.
   * @param t
   */
  Wave *pause(seconds t) {
    return pause_samples(t*sample_rate);
  }
  Wave *pause_samples(int len) {
    Wave *p = new Wave(len);
    for(int i=0; i<len; i++) {
      (*p)[i]=0;
    }
    return std::move(p);
  }

  /**
   * Write little endian, 4 byte ra array to file.
   */
  void save(path name, Wave &content) {
    FILE *file = fopen(name, "wb");
    fwrite(content.get_arr(), sizeof(float), content.get_len(), file);
    fclose(file);
    // free(file); // am i retarded?
  }

  /**
   * Play raw audio file with ffmpeg
   */
  void play(path name) {
    char f[64];
    sprintf(f, "ffplay -f f32le -ar %f %s", sample_rate, name);
    system(f);
  }
}
