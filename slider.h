#pragma once
#include <qdrawutil.h>
#include <QPainter>
#include <QPalette>

#define private protected
#include <qwt/QwtSlider>
#undef private

class QwtSlider::PrivateData
{
  public:
    PrivateData()
        : repeatTimerId( 0 )
        , updateInterval( 150 )
        , stepsIncrement( 0 )
        , pendingValueChange( false )
        , borderWidth( 2 )
        , spacing( 4 )
        , scalePosition( QwtSlider::TrailingScale )
        , hasTrough( true )
        , hasGroove( false )
        , mouseOffset( 0 )
    {
    }

    int repeatTimerId;
    bool timerTick;
    int updateInterval;
    int stepsIncrement;
    bool pendingValueChange;

    QRect sliderRect;

    QSize handleSize;
    int borderWidth;
    int spacing;

    Qt::Orientation orientation;
    QwtSlider::ScalePosition scalePosition;

    bool hasTrough;
    bool hasGroove;

    int mouseOffset;

    mutable QSize sizeHintCache;
};

class Slider : public QwtSlider
{
protected:
  void drawHandle(QPainter *painter, const QRect& handleRect, int pos) const override
  {
    const int bw = m_data->borderWidth;

    QColor color = palette().color(QPalette::Button);
    color.setAlpha(.5);
    QPalette new_palette = QPalette(color);
    qDrawShadePanel( painter,
        handleRect, palette(), false, bw,
        &new_palette.brush( QPalette::Button ) );

    pos++; // shade line points one pixel below
    if ( orientation() == Qt::Horizontal )
    {
        qDrawShadeLine( painter, pos, handleRect.top() + bw,
            pos, handleRect.bottom() - bw, palette(), true, 1 );
    }
    else // Vertical
    {
        qDrawShadeLine( painter, handleRect.left() + bw, pos,
            handleRect.right() - bw, pos, palette(), true, 1 );
    }
  }
public:
  Slider(QWidget *parent = nullptr) : QwtSlider(parent) {};
};

