#pragma once
#include "defs.h"
#include "instrument.h"

class Wave {
  public:
    explicit Wave(int);
    explicit Wave(wave_t, int);
    ~Wave(void);
    int get_len(void) const;
    const pulse &operator[](int i) const { return arr[i]; };
    pulse &operator[](int i) { return arr[i]; }
    void set_len(int);
    wave_t get_arr(void) const { return arr;}
    void set_arr(wave_t, int);
    void add_to_end(const Wave *other);
    void del(void);
    void resize(int);
    void add(Wave *);
    typedef wave_t iterator;
    iterator begin() const
    {
      return &arr[0];
    }
    iterator end() const
    {
      return &arr[n_samples - 1];
    }
    bool nofree;
  private:
    int n_samples;
    wave_t arr;
    bool freed;
};

namespace wave {
  struct wave_region {
    int offset;
    int length;
    seconds note_length;
  };

  hz st(semitones n);
  pulse ft(seconds x, mod_const k, frequency fm);
  Wave ft(seconds *x, mod_const k, frequency fm);
  ang_vel ω(frequency x);
  void add(std::vector<Wave> &ws);
  Wave *make_wave(hz freq, wave_region t, volume vol, Instrument *i);
  Wave *pause(seconds t=1);
  Wave *pause_samples(int t);
  void save(path name, Wave &content);
  void play(path name);
}

class Frame : public Wave /**< 1024-sample long section of a wave */
{
  public:
  Frame() :Wave(frame_size)
  {}
  Frame(wave_t a) :Wave(a, frame_size)
  {}
  void del()
  {}
};

class Envelope : public Wave
{
  public:
  Envelope(int n_frames) :Wave(n_frames)
  {}
};
