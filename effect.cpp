#include <pipewire/pipewire.h>
#include <QCoreApplication>
#include <QThread>

#include "effect.h"
#include "qt.h"

Effect::Effect(const char *name) :Source(name)
{

  m_input_port = static_cast<struct port *>(pw_filter_add_port(m_filter,
      PW_DIRECTION_INPUT,
      PW_FILTER_PORT_FLAG_MAP_BUFFERS,
      sizeof(struct port),
      pw_properties_new(
        PW_KEY_FORMAT_DSP, "32 bit float mono audio",
        PW_KEY_PORT_NAME, "input",
        NULL),
      NULL, 0));
}

Effect::~Effect() {
  pw_filter_destroy(m_filter);
}

void Effect::set_gain(float gain)
{
  async_update();
  QMutexLocker l(&m_parameter_lock);
  m_gain = gain;
}

void Effect::set_mix(float mix)
{
  async_update();
  QMutexLocker l(&m_parameter_lock);
  m_mix = mix;
}

void Effect::set_preamp(float preamp)
{
  async_update();
  QMutexLocker l(&m_parameter_lock);
  m_preamp = preamp;
}
