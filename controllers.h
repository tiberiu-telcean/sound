#pragma once
#include <QObject>

#include "dB.h"
#include "main_client.h"
#include "synth.h"

class Win;

class Controller : public QObject
{
  Q_OBJECT
protected:
  Win *m_window;
  void stop(int signal);
public slots:
  virtual void run() {};
  virtual void set_volume(float) {}
public:
  Controller(Win *window) :m_window(window) {}

  virtual uint32_t get_node_id() const { return 0; }
  virtual bool is_running() const { return false; }
  virtual void window_closed() {}
};

class MainClientController : public Controller
{
  Q_OBJECT
public slots:
  void run() { m_main_client->run(); }
  void set_output(int index) { if(index >= 0) m_main_client->set_output(index); }
  void set_master_volume(int x) { m_main_client->set_master_volume(dB_to_amplitude(x)); }
  void set_track_volume(const char *track_id, int x) { m_main_client->set_track_volume(track_id, dB_to_amplitude(x)); }
  void load_midi_file(const char *filename) {
    m_main_client->load_midi_file(filename);
  };
signals:
  void new_output_device();
protected:
  void new_sink() { emit new_output_device(); }
  void update_scope(std::shared_ptr<float[]> data, int size);
  friend MainClient;
public:
  MainClient *m_main_client;
  const std::vector<const Output*> get_outputs() const { return m_main_client->get_output_list(); }
  const char *create_track() { return std::move(m_main_client->create_track()); }
  MainClientController(Win *window) :Controller(window), m_main_client(new MainClient(this)) {};
};

class SynthController : public Controller
{
  Q_OBJECT
public slots:
  void run() { m_synth->run(); }
  void set_volume(float x) override { m_synth->set_volume(x); }
protected:
  void progress(int x);
  void set_length(int seconds);
  int getInstrument() const;
  friend Synth;
public:
  Synth *m_synth;
  SynthController(Win *window) :Controller(window), m_synth(new Synth(this)) {}

  uint32_t get_node_id() const { return m_synth->get_node_id(); }
  bool is_running() const { return m_synth->is_running(); }
  void window_closed() { m_synth->window_closed(); }
  void key_pressed(int key) { m_synth->key_pressed(key); }
  void key_released(int key) { m_synth->key_released(key); }
};
