#pragma once
#include <functional>
#include <QMutex>
#include <type_traits>

template<typename T>
struct identity { typedef T type; };

class Mutex : public QMutex {
public:
  template<typename T>
  T with_lock_acquired(std::function<T ()> callback)
  {
    return with_lock_acquired(callback, identity<T>());
  }
private:
  template<typename T>
  T with_lock_acquired(std::function<T ()> callback, identity<T>)
  {
    T result;
    lock();
    result = callback();
    unlock();
    return result;
  }

  void with_lock_acquired(std::function<void ()> callback, identity<void>)
  {
    lock();
    callback();
    unlock();
  }
};
