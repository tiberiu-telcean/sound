#pragma once
#include <cstdio>
#include <QAbstractEventDispatcher>
#include <QApplication>

extern QApplication *app;
extern FILE *output;

template <typename F>
static void postToThread(F && fun, QThread *thread) {
   auto *obj = QAbstractEventDispatcher::instance(thread);
   Q_ASSERT(obj);
   QMetaObject::invokeMethod(obj, std::forward<F>(fun));
}
