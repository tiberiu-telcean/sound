#pragma once
#include <QMutex>
#include <spa/param/latency-utils.h>

#include "wave.h"
#include "client.h"


class EffectController;

class Effect : public Source {
protected:
  struct port *m_input_port;
  EffectController *m_controller;

  float m_mix = 1;
  float m_gain = 0;
  float m_preamp = 0;
  QMutex m_parameter_lock;

public:
  virtual void set_gain(float);
  virtual void set_mix(float);
  virtual void set_preamp(float);
  Effect(const char *name="effect");
  ~Effect();
};
